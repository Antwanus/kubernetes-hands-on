/**     Getting to the host of our minikube  */

PS D:\work\Kubernetes Hands-On\Course+Files+v7\My Workspace\microservices> minikube ssh
                                _             _
                    _         _ ( )           ( )
        ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __
        /' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
        | ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
        (_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ login
        minikube login: docker
        Password: tcuser
                                _             _
                    _         _ ( )           ( )
        ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __
        /' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
        | ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
        (_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ ls
$ pwd
        /home/docker
$ cd /
$ ls
        bin   dev  home  lib    libexec  media  opt   root  sbin  sys  usr
        data  etc  init  lib64  linuxrc  mnt    proc  run   srv   tmp  var
$ cd mnt
$ ls
        mein  sda1
$ cd mein
$ ls
        mongodb
$ cd mongodb
$ ls
        storage
$ cd storage
$ ls
        data
$ cd data
$ pwd
        /mnt/mein/mongodb/storage/data
$ ls
        WiredTiger                            diagnostic.data
        WiredTiger.lock                       index-1--6654709968677348903.wt
        WiredTiger.turtle                     index-3--6654709968677348903.wt
        WiredTiger.wt                         index-5--6654709968677348903.wt
        WiredTigerLAS.wt                      index-7--6654709968677348903.wt
        _mdb_catalog.wt                       index-8--6654709968677348903.wt
        collection-0--6654709968677348903.wt  journal
        collection-2--6654709968677348903.wt  mongod.lock
        collection-4--6654709968677348903.wt  sizeStorer.wt
        collection-6--6654709968677348903.wt  storage.bson